import * as express from 'express';
import * as bodyParser from 'body-parser';

import { AppRoutes } from './routes/AppRoutes';

class App {
    public app: express.Application;
    public appRoutes: AppRoutes = new AppRoutes();

    constructor() {
        this.app = express();
        this.config();
    }

    private config(): void {
        this.app.use(bodyParser.json());
        this.app.use(bodyParser.urlencoded({extended: false}));

        this.app.use(function(req, res, next) {
            res.setHeader("Access-Control-Allow-Origin", "*");
            res.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
            next();
        });

        this.appRoutes.routes(this.app);
    }
}

export default new App().app;