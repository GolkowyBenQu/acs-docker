import {AppController} from "../controllers/AppController";
import {Request, Response} from "express";

export class AppRoutes
{
    private appController: AppController = new AppController();

    public routes(app: any): void
    {
        app.route('/')
        .get((req: Request, res: Response) => {
            return this.appController.homepage(req, res);
        });

        app.route('/containers')
        .get((req: Request, res: Response) => {
            return this.appController.containers(req, res);
        });

        app.route('/containers/:id/reset')
        .get((req: Request, res: Response) => {
            return this.appController.restartContainer(req, res);
        });
    }
}