import {Request, Response} from 'express';

export class AppController
{
    public apiClient: any = null;
    private config = {
        socketPath: '/var/run/docker.sock'
    };

    constructor()
    {
        this.apiClient = require('axios');
    }

    public homepage(req: Request, res: Response)
    {
        this.apiClient.get('/info', this.config)
        .then(function (response: any) {
            res.send({response: response.data});
        })
        .catch(function (err: any) {
            console.log(err);
        });
    }

    public containers(req: Request, res: Response)
    {
        this.apiClient.get('/containers/json?all=true', this.config)
        .then(function (response: any) {
            res.send({response: response.data});
        })
        .catch(function (err: any) {
            console.log(err);
        });
    }

    public inspectContainer(req: Request, res: Response)
    {
        const id = 1;
        this.apiClient.get('/containers/' + id + '/json', this.config)
        .then(function (response: any) {
            res.send({response: response.data});
        })
        .catch(function (err: any) {
            console.log(err);
        });
    }

    public listProcesses(req: Request, res: Response)
    {
        const id = 1;
        this.apiClient.get('/containers/' + id + '/top', this.config)
        .then(function (response: any) {
            res.send({response: response.data});
        })
        .catch(function (err: any) {
            console.log(err);
        });
    }

    public getLogs(req: Request, res: Response)
    {
        const id = 1;
        this.apiClient.get('/containers/' + id + '/logs', this.config)
        .then(function (response: any) {
            res.send({response: response.data});
        })
        .catch(function (err: any) {
            console.log(err);
        });
    }

    public getStats(req: Request, res: Response)
    {
        const id = 1;
        this.apiClient.get('/containers/' + id + '/stats', this.config)
        .then(function (response: any) {
            res.send({response: response.data});
        })
        .catch(function (err: any) {
            console.log(err);
        });
    }

    public startContainer(req: Request, res: Response)
    {
        const id = 1;
        this.apiClient.post('/containers/' + id + '/start', this.config)
        .then(function (response: any) {
            res.send({response: response.data});
        })
        .catch(function (err: any) {
            console.log(err);
        });
    }

    public stopContainer(req: Request, res: Response)
    {
        const id = 1;
        this.apiClient.post('/containers/' + id + '/stop', this.config)
        .then(function (response: any) {
            res.send({response: response.data});
        })
        .catch(function (err: any) {
            console.log(err);
        });
    }

    public restartContainer(req: Request, res: Response)
    {
        const url = '/containers/' + req.params.id + '/restart';

        this.apiClient.post(url, {}, this.config)
        .then(function (response: any) {
            res.send({response: response.data});
        })
        .catch(function (err: any) {
            res.status(500).send(err);
        });
    }
}