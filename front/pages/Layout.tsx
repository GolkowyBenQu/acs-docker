import * as React from 'react';
import {BrowserRouter as Router, Route, Link} from "react-router-dom";

import HomePage from "./HomePage";
import ContainersPage from "./ContainersPage";
import ContainerDetailsPage from "./ContainerDetailsPage";

class Layout extends React.Component<any, any>
{
    constructor(props: any)
    {
        super(props);
    }

    public render()
    {
        return <Router>
            <div className='wrapper'>
                <div className='sidebar' data-color="purple" data-background-color="white">
                    <div className='logo'>
                        <a href='#' className='simple-text logo-normal'>ACS Docker v2.0</a>
                    </div>
                    <div className='sidebar-wrapper'>
                        <ul className='nav'>
                            <li className='nav-item active'>
                                <Link to='/' className='nav-link'>
                                    <i className='material-icons'>dashboard</i>
                                    <p>dashboard</p>
                                </Link>
                            </li>
                            <li className='nav-item active'>
                                <Link to='/containers' className='nav-link'>
                                    <i className='material-icons'>all_inbox</i>
                                    <p>containers</p>
                                </Link>
                            </li>
                        </ul>
                    </div>
                </div>
                <div className='main-panel'>
                    <nav className='navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top'>
                        <div className='container-fluid'>
                            <div className='navbar-wrapper'>
                                <a href='#' className='navbar-brand'>title</a>
                            </div>
                        </div>
                    </nav>
                    <div className='content'>
                        <div className='container-fluid'>
                            <Route exact path="/" component={HomePage}/>
                            <Route path="/containers" component={ContainersPage}/>
                            <Route path='/container/:id' component={ContainerDetailsPage} />
                        </div>
                    </div>
                    <footer className='footer'>
                        <div className='container-fluid'>
                            <nav className='float-left'>
                                <ul>
                                    <li>
                                        <a href='#'>Apsensa.pl</a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </footer>
                </div>
            </div>
        </Router>;
    }
}

export default Layout;