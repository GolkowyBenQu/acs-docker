import * as React from 'react';
import {connect} from "react-redux";

import Layout from './Layout';
import CardStat from './../components/CardStat';
import {dockerActions} from '../actions/dockerActions';

class HomePage extends React.Component<any, any>
{
    constructor(props: any, state: any)
    {
        super(props, state);
    }

    componentDidMount()
    {
        this.props.dispatch(dockerActions.getInfo())
    }

    public render()
    {
        const {info} = this.props;
        return <div>
            <div className='card'>
                <div className='card-header card-header-primary'>
                    <h4 className='card-title'>System info</h4>
                </div>
            </div>
            <div className='row'>
                {info &&
                <div className='col-lg-3'>
                    <CardStat
                        cardIcon='all_inbox'
                        cardType='card-header-info'
                        cardCategory='Containers'
                        value={info.Containers}
                    />
                </div>
                }
                {info &&
                <div className='col-lg-3'>
                    <CardStat
                        cardIcon='play_arrow'
                        cardType='card-header-success'
                        cardCategory='Running'
                        value={info.ContainersRunning}
                    />
                </div>
                }
                {info &&
                <div className='col-lg-3'>
                    <CardStat
                        cardIcon='pause'
                        cardType='card-header-warning'
                        cardCategory='Paused'
                        value={info.ContainersPaused}
                    />
                </div>
                }
                {info &&
                <div className='col-lg-3'>
                    <CardStat
                        cardIcon='stop'
                        cardType='card-header-danger'
                        cardCategory='Stopped'
                        value={info.ContainersStopped}
                    />
                </div>
                }
            </div>
        </div>;
    }
}

function mapStateToProps(state: any)
{
    const info = state.dockerReducer.info;
    return {info};
}

export default connect(mapStateToProps)(HomePage);