import * as React from 'react';

import TableRow from './../components/TableRow';
import {dockerActions} from "../actions/dockerActions";
import {connect} from "react-redux";

class ContainersPage extends React.Component<any, any>
{
    constructor(props: any, state: any)
    {
        super(props, state);
    }

    componentDidMount()
    {
        this.props.dispatch(dockerActions.getContainers());
    }

    public render()
    {
        let rows: any = [];

        {
            this.props.containers && this.props.containers.forEach((container: any) => {
                let containerObj = {
                    id: container.Id,
                    image: container.Image,
                    status: container.Status
                };
                rows.push(
                    <TableRow
                        container={containerObj}
                        key={container.Id}
                        resetCallback={this.props.resetContainer}
                    />
                );
            });
        }

        return <div className='card'>
            <div className='card-header card-header-primary'>
                <h4 className='card-title'>Containers</h4>
            </div>
            <div className='card-body'>
                <div className='table-responsive'>
                    <table className='table'>
                        <thead>
                        <tr>
                            <th>Id</th>
                            <th>Image</th>
                            <th>Status</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>{rows}</tbody>
                    </table>
                </div>
            </div>
        </div>;
    }
}

function mapStateToProps(state: any)
{
    const containers = state.dockerReducer.containers;
    return {containers};
}

function mapDispatchToProps(dispatch: any)
{
    return {
        resetContainer: (id: any) => {
            dispatch(dockerActions.resetContainer(id))
        },
        dispatch
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ContainersPage);