import {combineReducers} from "redux";
import {dockerReducer} from "./dockerReducer";

export const rootReducer = combineReducers({dockerReducer});