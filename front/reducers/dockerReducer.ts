import {dockerConst} from "../consts/dockerConst";

export function dockerReducer(state = {}, action: any) {
    switch(action.type) {
        case dockerConst.GET_INFO_SUCCESS:
            return {
                info: action.info
            };
        case dockerConst.GET_CONTAINERS_SUCCESS:
            return {
                containers: action.containers
            };
        case dockerConst.RESET_CONTAINER:
            return state;
        default:
            return state;
    }
}