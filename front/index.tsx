import * as React from 'react';
import * as ReactDom from 'react-dom';
import {Provider} from 'react-redux';

import Layout from "./pages/Layout";
import {store} from "./helpers/store";

ReactDom.render(
    <Provider store={store}>
        <Layout/>
    </Provider>,
    document.getElementById('app')
);