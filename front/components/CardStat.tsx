import * as React from 'react';

export default ({cardIcon, cardType, cardCategory, value}: any) => (
    <div className='card card-stats'>
        <div className={'card-header card-header-icon ' + cardType}>
            <div className='card-icon'>
                <i className='material-icons'>{cardIcon}</i>
            </div>
            <p className='card-category'>{cardCategory}</p>
            <h3 className='card-title'>{value}</h3>
        </div>
    </div>
)