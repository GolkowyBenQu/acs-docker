import * as React from 'react';
import {Link} from "react-router-dom";

export default ({container, resetCallback}: any) => (
    <tr>
        <td>{container.id}</td>
        <td>{container.image}</td>
        <td>{container.status}</td>
        <td>
            <Link to={'/container/' + container.id} className='btn'>
                details
            </Link>
            <button className={'btn'} onClick={() => {
                resetCallback(container.id);
                return false;
            }}>reset</button>
        </td>
    </tr>
)