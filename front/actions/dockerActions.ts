import {dockerConst} from "../consts/dockerConst";

export const dockerActions = {
    getInfo,
    getContainers,
    resetContainer
};

function getInfo()
{
    return (dispatch: any) => {
        dispatch(request());

        return fetch('http://127.0.0.1:3000/')
        .then(res => res.json())
        .then(res => {
            dispatch(success(res.response));
        })
        .catch(err => {
            dispatch(error(err))
        })
    };

    function request()
    {
        return {
            type: dockerConst.GET_INFO_REQUEST
        }
    }

    function success(info: any)
    {
        return {
            type: dockerConst.GET_INFO_SUCCESS,
            info: info
        }
    }

    function error(err: any)
    {
        return {
            type: dockerConst.GET_INFO_ERROR,
            error: err
        }
    }
}

function getContainers()
{
    return (dispatch: any) => {
        dispatch(request());

        return fetch('http://127.0.0.1:3000/containers')
        .then(res => res.json())
        .then(res => {
            dispatch(success(res.response))
        })
        .catch(err => {
            dispatch(error(err));
        })
    };

    function request()
    {
        return {
            type: dockerConst.GET_CONTAINERS_REQUEST
        }
    }

    function success(containers: any)
    {
        return {
            type: dockerConst.GET_CONTAINERS_SUCCESS,
            containers: containers
        }
    }

    function error(err: any)
    {
        return {
            type: dockerConst.GET_CONTAINERS_ERROR,
            error: err
        }
    }
}

function resetContainer(id: any)
{
    return (dispatch: any) => {
        dispatch(request());

        return fetch('http://127.0.0.1:3000/containers/' + id + '/reset')
        .then(res => res.json())
        .then(res => {
            dispatch(success(res.response))
        })
        .catch(err => {
            dispatch(error(err));
        })
    };

    function request()
    {
        return {
            type: dockerConst.GET_CONTAINERS_REQUEST
        }
    }

    function success(containers: any)
    {
        return {
            type: dockerConst.GET_CONTAINERS_SUCCESS,
            containers: containers
        }
    }

    function error(err: any)
    {
        return {
            type: dockerConst.GET_CONTAINERS_ERROR,
            error: err
        }
    }
}